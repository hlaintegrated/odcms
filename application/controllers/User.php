<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function index()
	{
		redirect("main");
	}

	public function login()
        {
		// get form input
		$email = $this->input->post("email");
        $password = $this->input->post("password");

		// form validation
		$this->form_validation->set_rules("email", "Email");
		$this->form_validation->set_rules("password", "Password", "md5");



		if ($this->form_validation->run() == FALSE)
        {
			// validation fail
			$this->load->view('dashboard');
		}
		else
		{   
			$checkuser =  $this->user_model->get_user($email,md5($password));
            if($checkuser == TRUE) {

                       $sess_data = array('login' => TRUE,'email' => $email);
				       $this->session->set_userdata($sess_data);
				       redirect("admin");

					   } else {

                       $this->session->set_flashdata('msg', '<div class="alert alert-danger text-center">Wrong Email or Password!</div>');
                       redirect('main/access');}

		  }


		}

	public function logout()
	{
		//to do
	}

	public function create()
	{
		//to do
	}

	public function update()
	{
		//to do
	}

	public function delete()
	{
		//to do
	}

	public function get_user()
	{
		//to do
	}

	public function get_users()
	{
		//to do
	}
	  

  


}
