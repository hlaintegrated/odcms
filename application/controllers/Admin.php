<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	
	public function index()
	{
		$data = [];
		$data["section"] = "dashboard";
		$this->template->load('template_admin', 'dashboard_view', $data);
	}
	
	public function page()
	{
		$data = [];
		$cid = $this->uri->segment(3);
		//$data['content'] =  $this->content_model->get_content($cid);
		$data["section"] = "page";
		$this->template->load('template_admin', 'dashboard_view', $data);
	}

	public function article()
	{
		$data = [];
		$cid = $this->uri->segment(3);
		//$data['content'] =  $this->content_model->get_content($cid);
		$data["section"] = "article";
		$this->template->load('template_admin', 'dashboard_view', $data);
	}

	public function faq()
	{
		$data = [];
		$cid = $this->uri->segment(3);
		//$data['content'] =  $this->content_model->get_content($cid);
		$data["section"] = "faq";
		$this->template->load('template_admin', 'dashboard_view', $data);
	}

	public function announcement()
	{
		$data = [];
		$cid = $this->uri->segment(3);
		//$data['content'] =  $this->content_model->get_content($cid);
		$data["section"] = "announcement";
		$this->template->load('template_admin', 'dashboard_view', $data);
	}

	public function image()
	{
		$data = [];
		$cid = $this->uri->segment(3);
		//$data['content'] =  $this->content_model->get_content($cid);
		$data["section"] = "image";
		$this->template->load('template_admin', 'dashboard_view', $data);
	}

	public function ticket()
	{
		$data = [];
		$cid = $this->uri->segment(3);
		//$data['content'] =  $this->content_model->get_content($cid);
		$data["section"] = "ticket";
		$this->template->load('template_admin', 'dashboard_view', $data);
	}

	public function user()
	{
		$data = [];
		$cid = $this->uri->segment(3);
		//$data['content'] =  $this->content_model->get_content($cid);
		$data["section"] = "user";
		$this->template->load('template_admin', 'dashboard_view', $data);
	}

	public function audit()
	{
		$data = [];
		$cid = $this->uri->segment(3);
		//$data['content'] =  $this->content_model->get_content($cid);
		$data["section"] = "audit";
		$this->template->load('template_admin', 'dashboard_view', $data);
	}

	public function dataset()
	{
		$data = [];
		$cid = $this->uri->segment(3);
		//$data['content'] =  $this->content_model->get_content($cid);
		$data["section"] = "dataset";
		$this->template->load('template_admin', 'dashboard_view', $data);
	}

	public function selectdata()
	{
		$data = [];
		$cid = $this->uri->segment(3);
		//$data['content'] =  $this->content_model->get_content($cid);
		$data["section"] = "selectdata";
		$this->template->load('template_admin', 'dashboard_view', $data);
	}

	
	
}
