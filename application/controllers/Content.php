<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Content extends CI_Controller {

	
	public function index()
	{
		redirect("main");
	}
	
	public function page()
	{
		$cid = $this->uri->segment(3);
		if(empty($cid)){ redirect("main"); } // redirect if cid empty
		$data = [];
		$data['content'] =  $this->content_model->get_content($cid);
		$data["section"] = "page";
		$this->template->load('template_user', 'content_view', $data);
	}

	public function article()
	{
		$cid = $this->uri->segment(3);
		if(empty($cid)){ redirect("main"); } // redirect if cid empty
		$data = [];
		$data['content'] =  $this->content_model->get_content($cid);
		$data["section"] = "article"; // section article
		$this->template->load('template_user', 'content_view', $data);
	}

	public function galery()
	{
		$cid = $this->uri->segment(3);
		if(empty($cid)){ redirect("main"); } // redirect if cid empty
		$data = [];
		$data['content'] =  $this->content_model->get_content($cid);
		$data["section"] = "galery"; // section galery
		$this->template->load('template_user', 'content_view', $data);
	}

	  // sample crud
	  public function testcrud()
	  {
		 $this->ticket_model->crud('contents',$_POST);
	  }

	
	
}
