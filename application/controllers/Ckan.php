<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ckan extends CI_Controller {


public function index()
   {
        redirect("main");
   }


public function dataset()
   {
        $data = [];
        $data["section"] = "dataset";
        $this->template->load('template_user', 'ckan_view', $data);
   }

public function popular()
   {
        $data = [];
        $data["section"] = "dataset";
        $this->template->load('template_user', 'ckan_view', $data);
   }

public function latest()
   {
        $data = [];
        $data["section"] = "dataset";
        $this->template->load('template_user', 'ckan_view', $data);
   }

public function byorganisations()
   {
        $data = [];
        $data["section"] = "dataset";
        $this->template->load('template_user', 'ckan_view', $data);
   }

public function byapi()
   {
        $data = [];
        $data["section"] = "dataset";
        $this->template->load('template_user', 'ckan_view', $data);
   }

public function create_org()
   {
        //to do
   }
    
public function create_dataset()
   {
        //to do
   }

}