<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

  // front page 
  public function index()
	{
		$data = [];
		$this->template->load('template_user', 'home_view', $data);
  }

  // admin login page
  public function access()
	{
		$this->load->view('access');
  }

  // sample crud
 public function testcrud()
 {
    $this->ticket_model->crud('testcases',$_POST);
 }



}
