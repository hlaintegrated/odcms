<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ticket extends CI_Controller {


	public function index()
	{
		$data = [];
		$data["section"] = "new";
        $data['sources'] = $this->ticket_model->get_all_sources();
        $this->template->load('template_user', 'ticket_view', $data);
    }
    // semak tiket
    public function check()
	{
		$data = [];
		$data["section"] = "check";
        $data['sources'] = $this->ticket_model->get_all_sources();
        $this->template->load('template_user', 'ticket_view', $data);
	}
    
    //submit dataset request
    public function addrequest()
    { 
        $date = date('my');
        $data = $this->ticketnumber();

        if (!empty($_POST)) {
            $request_by = $this->input->post('request_by');
            $contact = $this->input->post('contact');
            $useremail = $this->input->post('email');
            $title = $this->input->post('title');
            $description = $this->input->post('description');
            $justification = $this->input->post('justification');
            $organisation = $this->input->post('oid');

            // Checking if everything is there
            if ($request_by && $contact && $useremail && $title && $description && $justification && $organisation) {
                // Loading model
                $this->load->model('ticket_model');
                $ticket = array(
                    'ticket_num'=>$data,
                    'request_by'=>$request_by,
                    'contact'=>$contact,
                    'email'=>$useremail,
                    'title'=>$title,
                    'description'=>$description,
                    'justification'=>$justification,
                    'oid'=>$organisation,
                    'stid'=>'1', //status id (1) - new status
                );

                // Call sendemail function tu send email
                $mail = $this->sendemail($ticket); 

                // Calling model to add ticket
                $add = $this->ticket_model->crud('tickets',$ticket);
            }
        }
    }

    public function ticketnumber()
    {
        $date = date('my');
        $checked_ticket = $this->ticket_model->check_ticket_number($date);
        
        if($checked_ticket == null){
            $newticket = $date."0001";
            return $newticket;
        }
        else{
            $existedticket = str_pad($checked_ticket+1, 8, '0', STR_PAD_LEFT);
            return $existedticket;
        }

    }
     // sample crud
    public function testcrud()
     {
         $this->ticket_model->crud('tickets',$_POST);
     }

     public function sendemail($ticket)
     {
      $this->load->library('email');
      //$this->load->library('encrypt');
      //$useremail = $this->uri->segment(3);
      $config = array(
             'protocol' => 'smtp', 
             'smtp_host' => 'ssl://smtp.googlemail.com', 
             'smtp_port' => 465,
             'smtp_timeout' => 20,
             'smtp_user' => 'hlaintegrated@gmail.com', 
             'smtp_pass' => '@Empatbelas14', 
             'mailtype' => 'html', 
             'charset' => 'iso-8859-1');
     $this->email->initialize($config);
     $this->email->set_mailtype("html");
     $this->email->set_newline("\r\n");

     $this->email->to($ticket['email']);
     $this->email->from('noreply@dev.data.gov.my','Data Terbuka MAMPU');
     $this->email->subject('Permohonan Set Data');
     $data = [];
     $data["section"] = "email";
     $htmlContent = $this->load->view('ticket_view', $data, true);
     $this->email->message($htmlContent);
  
      //Send email
      $this->email->send();
      echo "email sent";
 
 }

	



}
