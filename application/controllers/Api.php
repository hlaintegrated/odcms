<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Api extends REST_Controller {


		public function __construct()
	{
		parent::__construct();

	    $this->load->database();
		$this->load->model('api_model');

	    }



    public function token_post() {

		$checkactive = $this->api_model->GetUserData($this->post('username'));

		if (!empty($this->post('username')) && !empty($this->post('password')) && ($checkactive[0]['Active'] == 'Y')){

        $userValidate = $this->api_model->AuthenticateApiUserWithPassword($this->post('username'),$this->post('password'));

		$this->response([
              'token' => $userValidate[0]['Token']
              ], REST_Controller::HTTP_OK);

	   }  else {	       
		   $this->response(['status' => 'error','message' => 'missing username / password / account not active',
          ], REST_Controller::HTTP_OK);
	   }
	}

	public function status_post() {

	    
		$userDetails = $this->getHeaderToken();
		
		if (!empty($this->post('LmiRequestId')) && !empty($userDetails)){
        
		$RequestStatus = $this->api_model->GetRequestStatus($this->post('LmiRequestId'));
  
		$this->response([
              'status' => $RequestStatus[0]['RequestStatusDesc'],
              'lastupdate' => $RequestStatus[0]['LastModifiedOn']
              ], REST_Controller::HTTP_OK);

	    }  else {	       
		   $this->response(['status' => 'error','message' => 'missing LmiRequestId or invalid Token',
          ], REST_Controller::HTTP_OK);
	   }


	}


	public function request_post() {
        
		if (!empty($this->post('brandId')) && !empty($this->post('memberIds'))){

        $userDetails = $this->getHeaderToken();
  
        if (!empty($userDetails['Login'])){
        
		$MemberIds = $this->post('memberIds');

		$RequestId = date("YdmAihs").rand(10,100);
		$RequestCreateStatus = $this->api_model->CreateNewRequest($RequestId,'MEMBER-LIST-IE',$this->post('brandId'),$userDetails['Login']);

		foreach ($MemberIds as $MemberId) {
		$AppendMemberIdStatus = $this->api_model->AppendMemberToRequest($RequestId,$MemberId);

		}

		$SubmitRequestStatus = $this->api_model->SubmitRequest($RequestId);

        } else {

		  $this->response(['status' => 'error','message' => 'invalid token',
          ], REST_Controller::HTTP_OK);
        }

      
		$this->response([
              'lmirequestid' => $RequestId,
              ], REST_Controller::HTTP_OK);

		  }  else {	       
		   $this->response(['status' => 'error','message' => 'missing brandId or memberIds',
          ], REST_Controller::HTTP_OK);
	   }


	}



	public function getHeaderToken() {

           foreach (getallheaders() as $name => $value) {
			   if($name == 'Authorization'){ $headerVal = explode(" ",$value); 
			         } 
			       }
				   
			       $validateUserToken = $this->api_model->AuthenticateApiUserWithToken($headerVal[1]);

				   if (count($validateUserToken) > 0 ){

				         $TypePermission = explode("_",$validateUserToken[0]['PermissionCode']);
						 $data['TypePermission'] = $TypePermission[1];
						 $data['Login'] = $validateUserToken[0]['Login'];
				         return $data; 

				   }
			   
           	
          } 
}

