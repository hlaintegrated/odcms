<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Api_model extends CI_Model {

    public function __construct() {
        parent::__construct();
		 $this->load->database();
		 $this->load->helper('file');
		 $this->load->dbutil();
    }

	public function getUser($email)
      {
       
	   $query = $this->db->query("Select * MdmExportApi.Users Where Login = ".$email);
       return $query->result_array();
       }

	public function AuthenticateApiUserWithPassword($email,$password)
       {
       
	   $query = $this->db->query("EXEC MdmExportApi.AuthenticateApiUserWithPassword'".$email."','".$password."' ");
       return $query->result_array();
       }


	public function GetUserData($email)
       {
       
	   $query = $this->db->query("EXEC MdmExportApi.GetUserData'".$email."' ");
       return $query->result_array();
       }



	public function AuthenticateApiUserWithToken($userToken)
      {
       
	   $query = $this->db->query("EXEC MdmExportApi.AuthenticateApiUserWithToken'".$userToken."' ");
       return $query->result_array();
      }



	public function CreateUser($email,$password,$fullname)
       {
       
	   $query = $this->db->query("EXEC MdmExportApi.CreateUser'".$email."','".$password."','".$fullname."' ");
       return $query->result_array();
       }

	public function AddPermission($email)
       {
       
	   $query = $this->db->query("EXEC MdmExportApi.AddPermission'".$email."','USE_MEMBER-LIST-IE' ");
       return $query->result_array();
       }

	public function ModifyUser($email,$password,$fullname)
       {
       
	   $query = $this->db->query("EXEC MdmExportApi.ModifyUser'".$email."','".$password."','".$fullname."' ");
       return $query->result_array();
       }

   public function ChangeUserActiveStatus($email,$status)
    {

	  $query = $this->db->query("EXEC MdmExportApi.ChangeUserActiveStatus'".$email."','".$status."' ");
       return $query->result_array();
	   
    }


	
	public function CreateNewRequest($RequestId,$TypePermission,$BrandId,$Login)
       {
       
	   $query = $this->db->query("EXEC MdmExportApi.CreateNewRequest'".$RequestId."','".$TypePermission."','".$BrandId."','".$Login."' ");
       return $query->result_array();
       }

	public function AppendMemberToRequest($RequestId,$MemberId)
       {
       
	   $query = $this->db->query("EXEC MdmExportApi.AppendMemberToRequest'".$RequestId."','".$MemberId."' ");
       return $query->result_array();
       }

	public function SubmitRequest($RequestId)
       {
       
	   $query = $this->db->query("EXEC MdmExportApi.SubmitRequest'".$RequestId."' ");
       return $query->result_array();
       }


	public function GetListOfRequestsReadyToSend()
       {
       
	   $query = $this->db->query("EXEC MdmExportApi.GetListOfRequestsReadyToSend ");
       return $query->result_array();
       }


	public function GetRequestStatus($RequestId)
       {
       
	   $query = $this->db->query("EXEC MdmExportApi.GetRequestStatus'".$RequestId."' ");
       return $query->result_array();
       }


	public function GetInternalReport($RequestId)
       {
       
	   $query = $this->db->query("EXEC MdmExportApi.GetInternalReport'".$RequestId."' ");
     
       return $query->result_array();
       }

	public function GetExternalReport($RequestId)
       {
       
	   $query = $this->db->query("EXEC MdmExportApi.GetExternalReport'".$RequestId."' ");
       return $query->result_array();
       }

	public function FinishRequest($RequestId)
       {
       
	   $query = $this->db->query("EXEC MdmExportApi.FinishRequest'".$RequestId."' ");
       return $query->result_array();
       }


	public function RequestStatusChange($RequestId,$StatusCode,$Comments)
       {
       
	   $query = $this->db->query("EXEC MdmExportApi.RequestStatusChange'".$RequestId."','".$StatusCode."','".$Comments."' ");
       return $query->result_array();
       }

	public function IncreaseAttempts($RequestId)
       {
       
	   $query = $this->db->query("EXEC MdmExportApi.IncreaseAttempts'".$RequestId."' ");
       return $query->result_array();
       }

	public function AuthenticateUiUser($username,$password)
       {
       
	   $query = $this->db->query("EXEC MdmExportApi.AuthenticateUiUser'".$username."','".$password."' ");
       return $query->result_array();
       }

	public function GetUsersPermissions($username)
       {
       
	   $query = $this->db->query("EXEC MdmExportApi.GetUsersPermissions'".$username."' ");
       return $query->result_array();
       }


	public function GetAllUsers()
       {
       
	   $query = $this->db->query('SELECT * FROM MdmExportApi.Users');
       return $query->result_array();
       }

	public function GetUserId($userid)
    {
       if(!empty($userid)){
	   $query = $this->db->query('SELECT * FROM MdmExportApi.Users WHERE UserRecId ='.$userid);
       return $query->result_array();
	   }
    }


	public function GetAllRequests()
    {
      
	   $query = $this->db->query('SELECT * FROM MdmExportApi.Requests WHERE RequestRecId BETWEEN 1 AND 50 ORDER BY RequestRecId ASC');
       return $query->result_array();
	   
    }

     

}
?>

