<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Menu_model extends CI_Model {

  public function __construct() {
  parent::__construct();
	$this->load->database();
    
   }

    public function add_menu($menu)
    {
      $this->db->insert('menus', $menu);      
    }

    public function menu_list()
    {
      $query=$this->db->query("select * from menus");
      return $query->result();
    }

    public function user_list()
    {
      $query=$this->db->query("select * from users");
      return $query->result();
    }

    function deleterecords($id)
    {
      $this->db->query("delete from announcement where announcement_id='".$id."'");
    }

    public function list_announce()
    {
      $query=$this->db->query("select * from announcement");
      return $query->result();    
    }

    public function add_question($question)
    {
      $this->db->insert('questions', $question);      
    }

    public function ticket_list()
    {
      $query=$this->db->query("select * from tickets");
      return $query->result();
    }

    public function question_list()
    {
      $query=$this->db->query("select * from questions");
      return $query->result();
    }
    
}
?>