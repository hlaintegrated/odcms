<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Ticket_model extends CI_Model {

    public function __construct() {
    parent::__construct();
	  $this->load->database();
    
   }

	public function get_all_tickets()
    {

      $query = $this->db->query("SELECT * FROM tickets limit 10 "); 
      return $query->result_array();
	   
    }

  public function get_all_sources()
    {

      $query = $this->db->query("SELECT oid,name_bm FROM organisations"); 
      return $query->result();
	   
    }
    
  public function check_ticket_number($date)
    {
      $this->db->select_max('ticket_num')->from('tickets')->where("ticket_num LIKE '{$date}%'");
      $query = $this->db->get();

      if ($query->num_rows() > 0) {
          return $query->row()->ticket_num;
      }
      return false;
    }

    // function for insert,update and delete
    public function crud($table,$data)
       { 
          $this->db->replace($table,$data);
          $insertId = $this->db->insert_id();
          return $insertId;
       }

}
?>

