<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Content_model extends CI_Model {

  public function __construct() {
  parent::__construct();
	$this->load->database();
    
   }
    // dapatkan content guna cid
    public function get_content($cid)
    {
      $query=$this->db->query("SELECT * FROM contents WHERE cid =".$cid);
      return $query->result_array();     
    }
    // senarai semua contents limit 20
    public function get_contents()
    {
      $query=$this->db->query("SELECT * FROM contents LIMIT 20");
      return $query->result();
    }

    // function for insert,update and delete
    public function crud($table,$data)
	  {
      
       $this->db->replace($table,$data);
       $insertId = $this->db->insert_id();
       return $insertId;
	  }
     
}
?>