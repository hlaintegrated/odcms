<!doctype html>
<html lang="en">
  <head>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>MAMPU - Data Terbuka Sektor Awam</title>

    <!-- Custom fonts for this template-->
        <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!-- <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script> -->

    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.min.js" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Round|Material+Icons+Sharp">
    <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,700&display=swap" rel="stylesheet">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <link href="<?php echo base_url();?>assets/css/portal.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>assets/css/hover.css" rel="stylesheet" type="text/css">
  </head>
  <body>
  <div class="container-fluid">
    <div class="row">
        <div class="col pr-top-line1">&nbsp;</div>
        <div class="col pr-top-line2">&nbsp;</div>
    </div>
</div>

<!-- Menu -->
<nav class="navbar navbar-light navbar-expand-lg py-2">
    <div class="container">
        <div class="navbar-brand mb-0 d-flex align-items-center">
            <a href="<?php echo base_url();?>"><img src="<?= base_url();?>assets/img/icon_only.png" style="width: 55px;" /></a>
            <div class="pl-3" style="display: inline-block;">
                <a href="<?php echo base_url();?>"><span class="pr-top-title">data.gov.my</span></a><br/>
                <!-- <span class="pr-top-subtitle">data untuk kesejahteraan rakyat</span> -->
            </div>
        </div>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link font-weight-bold" href="#">Pilihan Bahasa</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link font-weight-bold" href="<?php echo base_url();?>main/access">Log Masuk</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<nav class="navbar navbar-expand-lg pr-nav-bar">
    <div class="container">
        <div class="collapse navbar-collapse justify-content-center" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item px-4">
                    <a class="nav-link" href="http://150.242.183.95/data/dataset">Data</a>
                </li>
                <li class="nav-item dropdown px-4">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Perkhidmatan
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="<?php echo base_url();?>ticket/new">Permohonan Set Data</a>
                        <a class="dropdown-item" href="<?php echo base_url();?>ticket/check">Semakan Set Data</a>
                        <a class="dropdown-item" href="<?php echo base_url();?>content/page/1">Muat Turun Manual</a>
                    </div>
                </li>
                <li class="nav-item dropdown px-4">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Pengumuman
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="<?php echo base_url();?>content/page/1/en/terms">Hebahan</a>
                        <a class="dropdown-item" href="<?php echo base_url();?>content/page/1/en/terms">Aktiviti</a>
                    </div>
                </li>
                <li class="nav-item dropdown px-4">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Informasi
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="<?php echo base_url();?>content/page/1/en/terms">Terma Pengguna</a>
                        <a class="dropdown-item" href="<?php echo base_url();?>content/page/1/en/terms">Galeri Aplikasi</a>
                        <a class="dropdown-item" href="<?php echo base_url();?>content/page/1/en/terms">Peneraju Data Terbuka</a>
                        <a class="dropdown-item" href="<?php echo base_url();?>content/page/1/en/terms">Pekeliling Data Terbuka</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>
<?php echo $contents ?>
<footer class="pr-footer">
    <div class="container my-auto">
        <div class="row py-5">
            <div class="col-3">
                <a href="#" class="text-light">Data</a><br/>
                <a href="<?php echo base_url();?>content/page/1/en/terms" class="text-light">Mengenai data.gov.my</a><br/>
                <a href="<?php echo base_url();?>content/page/1/en/terms" class="text-light">Terma Penggunaan</a><br/>
                <a href="<?php echo base_url();?>content/page/1/en/terms" class="text-light">Peneraju Data Terbuka</a>
            </div>
            <div class="col-3">
                <a href="<?php echo base_url();?>content/page/1/en/terms" class="text-light">Dasar Privasi</a><br/>
                <a href="<?php echo base_url();?>content/page/1/en/terms" class="text-light">Polisi Data</a><br/>
                <a href="<?php echo base_url();?>content/page/1/en/terms" class="text-light">Soalan Lazim</a><br/>
                <a href="<?php echo base_url();?>content/page/1/en/terms" class="text-light">Penafian</a>
            </div>
            <div class="col-3">
                <a href="<?php echo base_url();?>content/page/1/en/terms" class="text-light">Galeri Aplikasi</a><br/>
                <a href="<?php echo base_url();?>content/page/1/en/terms" class="text-light">Permohonan Set Data</a><br/>
                <a href="<?php echo base_url();?>content/page/1/en/terms" class="text-light">Maklum Balas</a><br/>
                <a href="<?php echo base_url();?>content/page/1/en/terms" class="text-light">Log Masuk</a>
            </div>
            <div class="col-3">
                <a href="<?php echo base_url();?>content/page/1/en/terms" class="text-light">Hubungi Kami</a><br/>
                <a href="<?php echo base_url();?>content/page/1/en/terms" class="text-light">Pekeliling Data Terbuka</a><br/>
                <a href="<?php echo base_url();?>content/page/1/en/terms" class="text-light">Amalan kebebasan maklumat</a>
            </div>
        </div>
        <div class="copyright text-center my-auto pt-3 pb-5">
            <span class="text-muted">Copyright &copy; MAMPU 2020</span>
        </div>
    </div>
</footer>
<div class="container-fluid mb-0 pb-0">
    <div class="row">
        <div class="col pr-top-line1">&nbsp;</div>
        <div class="col pr-top-line2">&nbsp;</div>
    </div>
</div>


</body>

</html>
