<!doctype html>
<html lang="en">
  <head>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>MAMPU - Data Terbuka Sektor Awam</title>

    <!-- Custom fonts for this template-->
        <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!-- <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script> -->

    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.min.js" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Round|Material+Icons+Sharp">
    <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,700&display=swap" rel="stylesheet">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">

    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <link href="<?php echo base_url();?>assets/css/portal.css" rel="stylesheet" type="text/css">
    <link href="<?php echo base_url();?>assets/css/hover.css" rel="stylesheet" type="text/css">
  </head>
  <body>

  <div class="container-fluid">
    <div class="row">
        <div class="col pr-top-line1">&nbsp;</div>
        <div class="col pr-top-line2">&nbsp;</div>
    </div>
</div>

<!-- Menu -->
<nav class="navbar navbar-light navbar-expand-lg py-2">
    <div class="container">
        <div class="navbar-brand mb-0 d-flex align-items-center">
            <a href="<?php echo base_url();?>"><img src="<?= base_url();?>assets/img/dtsa-logo.png" style="width: 200px;" /></a>
            <div class="pl-3" style="display: inline-block;">
                <a href="<?php echo base_url();?>"><span class="pr-top-title"></span></a><br/>
                <!-- <span class="pr-top-subtitle">data untuk kesejahteraan rakyat</span> -->
            </div>
        </div>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav ml-auto">
            <li class="nav-item dropdown px-4">
            <a class="nav-link dropdown-toggle font-weight-bold" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Pilihan Bahasa
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="#">Bahasa Malaysia</a>
                        <a class="dropdown-item" href="#">English</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link font-weight-bold" data-toggle="modal" data-target="#myModal">Log Masuk</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
<nav class="navbar navbar-expand-lg pr-nav-bar">
    <div class="container">
        <div class="collapse navbar-collapse justify-content-center" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item dropdown px-4">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Data
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="<?php echo base_url();?>ckan/popular">Set Data Popular</a>
                        <a class="dropdown-item" href="<?php echo base_url();?>ckan/latest">Set Data Terkini</a>
                        <a class="dropdown-item" href="<?php echo base_url();?>ckan/byorganisations">Set Data Kementerian</a>
                        <a class="dropdown-item" href="<?php echo base_url();?>ckan/byapi">Set Data API</a>
                    </div>
                </li>
                <li class="nav-item dropdown px-4">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Perkhidmatan
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="<?php echo base_url();?>ticket">Permohonan Set Data</a>
                        <a class="dropdown-item" href="<?php echo base_url();?>ticket/check">Semakan Set Data</a>
                        <a class="dropdown-item" href="<?php echo base_url();?>content/page/1/">Muat Turun Manual</a>
                        <a class="dropdown-item" href="<?php echo base_url();?>content/page/1/">Galeri Aplikasi</a>
                    </div>
                </li>
                <li class="nav-item dropdown px-4">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Pengumuman
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="<?php echo base_url();?>content/page/1/">Hebahan</a>
                        <a class="dropdown-item" href="<?php echo base_url();?>content/page/1/">Aktiviti</a>
                    </div>
                </li>
                <li class="nav-item dropdown px-4">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Informasi
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="<?php echo base_url();?>content/page/1/">Artikel</a>
                        <a class="dropdown-item" href="<?php echo base_url();?>content/page/1/">Terma Penggunaan</a>
                        <a class="dropdown-item" href="<?php echo base_url();?>content/page/1/">Mengenai data.gov.my</a>
                        <a class="dropdown-item" href="<?php echo base_url();?>content/page/1/">Peneraju Data Terbuka</a>
                        <a class="dropdown-item" href="<?php echo base_url();?>content/page/1/">Pekeliling Data Terbuka</a>
                        <a class="dropdown-item" href="<?php echo base_url();?>content/page/1/">Polisi Data</a>
                        <a class="dropdown-item" href="<?php echo base_url();?>content/page/1/">Amalan Kebebasan Maklumat</a>
                        <a class="dropdown-item" href="<?php echo base_url();?>content/page/1/">Data Privasi</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>
<?php echo $contents ?>
<footer class="pr-footer">
    <div class="container my-auto">
        <div class="row py-5">
            <div class="col-3">
                <a href="<?php echo base_url();?>content/page/1/" class="text-light">Penafian</a>
            </div>
            <div class="col-3">
                <a href="<?php echo base_url();?>content/page/1/" class="text-light">Soalan Lazim</a><br/>
            </div>
            <div class="col-3">
                <a href="<?php echo base_url();?>content/page/1/" class="text-light">Maklum Balas</a><br/>
                <a href="<?php echo base_url();?>content/page/1/" class="text-light">Log Masuk</a>
            </div>
            <div class="col-3">
                <a href="<?php echo base_url();?>content/page/1/" class="text-light">Hubungi Kami</a><br/>
            </div>
        </div>
        <div class="copyright text-center my-auto pt-3 pb-5">
            <span class="text-muted">Copyright &copy; OPEN DATA 2015-2020</span><br>
            <span class="text-muted">Paparan terbaik menggunakan pelayar internet Mozilla Firefox, Chrome dan Internet Explorer 11 ke atas </span>
        </div>
    </div>
</footer>
<div class="container-fluid mb-0 pb-0">
    <div class="row">
        <div class="col pr-top-line1">&nbsp;</div>
        <div class="col pr-top-line2">&nbsp;</div>
    </div>
</div>

<ul class="nav bg-dark justify-content-center fixed-bottom">
  <li class="nav-item">
    <a class="nav-link active" href="#"><i class="material-icons-outlined">map</i></a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="#"><i class="material-icons-outlined">public</i></a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="#"><i class="material-icons-outlined">emoji_nature</i></a>
  </li>
  <li class="nav-item">
    <a class="nav-link disabled" href="#">Mobile Menu Test</a>
  </li>
</ul>

</body>



<!-- Login Modal -->
<div class="modal" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Login</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
      <form action="<?php echo base_url();?>user/login" method="post">
                  <form class="user">
                    <div class="form-group">
                      <input type="email" name="email" class="form-control form-control-user" id="exampleInputEmail" aria-describedby="emailHelp" placeholder="Enter Email Address..." value="test@gmail.com">
                    </div>
                    <div class="form-group">
                      <input type="password" name="password" class="form-control form-control-user" id="exampleInputPassword" placeholder="Password" value="12345">
                    </div>
                    <div class="form-group">
                      <div class="custom-control custom-checkbox small">
                        <input type="checkbox" class="custom-control-input" id="customCheck">
                        <label class="custom-control-label" for="customCheck">Remember Me</label>
                      </div>
                    </div>
                    <button aria-label="" class="btn btn-primary btn-lg m-t-10" type="submit">Sign in</button>
                    
                  </form>
      </div>


    </div>
  </div>
</div>

</html>
