
<div class="pr-search-bar" data-aos="fade-up">
    <div class="container p-5">
        <div class="row justify-content-center">
            <div class="col text-center">
                <h2 class="text-light p-2">Portal Data Terbuka Malaysia</h2>
                <!-- <h5 class="text-light p-2">Search for your public data here</h5> -->
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-md-8 col-sm-12 text-center py-4">
                <form>
                    <div class="form-group">
                        <div class="row">
                        <div class="col-lg-11">
                        <input type="text" class="form-control" placeholder="Search for your Malaysia Open Data/ Carian Set Data Terbuka Malaysia" aria-label="Search">
                        </div>
                        <button type="button" class="btn btn-light"><img src="assets/img/icon-search.png" width="20px" height="20px"></button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-12 text-center py-2 d-flex justify-content-center">
                <a href="http://150.242.183.95/data/dataset">
                <button type="button" class="btn btn-outline-light d-flex align-items-center">
                    <i class="material-icons-outlined mr-2">filter_list</i>
                    <span>Senarai Semua Set Data</span>
                </button>
                </a>
            </div>
        </div>
    </div>
    <svg style="pointer-events: none" class="wave" width="100%" height="50px" preserveAspectRatio="none" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 1920 75">
        <defs>
        <style>
                .a {
                fill: none;
                }

                .b {
                clip-path: url(#a);
                }

                .c,
                .d {
                fill: #f9f9fc;
                }

                .d {
                opacity: 0.5;
                isolation: isolate;
                }
            </style>
        <clipPath id="a">
        <rect class="a" width="1920" height="75"></rect>
        </clipPath>
        </defs>
        <title>wave</title>
        <g class="b">
        <path class="c" d="M1963,327H-105V65A2647.49,2647.49,0,0,1,431,19c217.7,3.5,239.6,30.8,470,36,297.3,6.7,367.5-36.2,642-28a2511.41,2511.41,0,0,1,420,48"></path>
        </g>
        <g class="b">
        <path class="d" d="M-127,404H1963V44c-140.1-28-343.3-46.7-566,22-75.5,23.3-118.5,45.9-162,64-48.6,20.2-404.7,128-784,0C355.2,97.7,341.6,78.3,235,50,86.6,10.6-41.8,6.9-127,10"></path>
        </g>
        <g class="b">
        <path class="d" d="M1979,462-155,446V106C251.8,20.2,576.6,15.9,805,30c167.4,10.3,322.3,32.9,680,56,207,13.4,378,20.3,494,24"></path>
        </g>
        <g class="b">
        <path class="d" d="M1998,484H-243V100c445.8,26.8,794.2-4.1,1035-39,141-20.4,231.1-40.1,378-45,349.6-11.6,636.7,73.8,828,150"></path>
        </g>
    </svg>
</div>

<div class="pr-kluster-bar my-5" data-aos="zoom-in-down">
    <div class="container">
        <div class="row justify-content-center">
            <div class="text-center my-3 pr-title">
                KLUSTER <span>DATA TERBUKA</span>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="text-center pr-kluster-item my-3">
            <button type="button" class="btn btn-default pr-kluster-button btn-lg mb-2 mx-auto">
                    <i class="material-icons-outlined">how_to_vote</i>
                    </button>
                Pilihanraya
            </div>
            <div class="text-center pr-kluster-item my-3">
                <button type="button" class="btn btn-default pr-kluster-button btn-lg mb-2 mx-auto">
                    <i class="material-icons-outlined">money</i>
                </button>
                Bajet
            </div>
            <div class="text-center pr-kluster-item my-3">
                <button type="button" class="btn btn-default pr-kluster-button btn-lg mb-2 mx-auto">
                    <i class="material-icons-outlined">people</i>
                </button>
                Bancian
            </div>
            <div class="text-center pr-kluster-item my-3">
                <button type="button" class="btn btn-default pr-kluster-button btn-lg mb-2 mx-auto">
                    <i class="material-icons-outlined">gavel</i>
                </button>
                Perundangan
            </div>
            <div class="text-center pr-kluster-item my-3">
                <button type="button" class="btn btn-default pr-kluster-button btn-lg mb-2 mx-auto">
                    <i class="material-icons-outlined">description</i>
                </button>
                Kontrak Awam
            </div>
            <div class="text-center pr-kluster-item my-3">
                <button type="button" class="btn btn-default pr-kluster-button btn-lg mb-2 mx-auto">
                    <i class="material-icons-outlined">attach_money</i>
                </button>
                Perbelanjaan Kerajaan
            </div>
            <div class="text-center pr-kluster-item my-3">
                <button type="button" class="btn btn-default pr-kluster-button btn-lg mb-2 mx-auto">
                    <i class="material-icons-outlined">terrain</i>
                </button>
                Pemilikan Tanah
            </div>
            <div class="text-center pr-kluster-item my-3">
                <button type="button" class="btn btn-default pr-kluster-button btn-lg mb-2 mx-auto">
                    <i class="material-icons-outlined">bar_chart</i>
                </button>
                Statistik Kebangsaan
            </div>
            <div class="text-center pr-kluster-item my-3">
                <button type="button" class="btn btn-default pr-kluster-button btn-lg mb-2 mx-auto">
                    <i class="material-icons-outlined">sports_kabaddi</i>
                </button>
                Jenayah
            </div>
            <div class="text-center pr-kluster-item my-3">
                <button type="button" class="btn btn-default pr-kluster-button btn-lg mb-2 mx-auto">
                    <i class="material-icons-outlined">local_hospital</i>
                </button>
                Kesihatan
            </div>
            <div class="text-center pr-kluster-item my-3">
                <button type="button" class="btn btn-default pr-kluster-button btn-lg mb-2 mx-auto">
                    <i class="material-icons-outlined">business</i>
                </button>
                Pendaftaran Syarikat
            </div>
            <div class="text-center pr-kluster-item my-3">
                <button type="button" class="btn btn-default pr-kluster-button btn-lg mb-2 mx-auto">
                    <i class="material-icons-outlined">map</i>
                </button>
                Pemetaan
            </div>
            <div class="text-center pr-kluster-item my-3">
                <button type="button" class="btn btn-default pr-kluster-button btn-lg mb-2 mx-auto">
                    <i class="material-icons-outlined">school</i>
                </button>
                Pendidikan
            </div>
            <div class="text-center pr-kluster-item my-3">
                <button type="button" class="btn btn-default pr-kluster-button btn-lg mb-2 mx-auto">
                    <i class="material-icons-outlined">eco</i>
                </button>
                Pertanian
            </div>
            <div class="text-center pr-kluster-item my-3">
                <button type="button" class="btn btn-default pr-kluster-button btn-lg mb-2 mx-auto">
                    <i class="material-icons-outlined">emoji_nature</i>
                </button>
                Alam Sekitar
            </div>
            <div class="text-center pr-kluster-item my-3">
                <button type="button" class="btn btn-default pr-kluster-button btn-lg mb-2 mx-auto">
                    <i class="material-icons-outlined">public</i>
                </button>
                Perdagangan Antarabangsa
            </div>
            <div class="text-center pr-kluster-item my-3">
                <button type="button" class="btn btn-default pr-kluster-button btn-lg mb-2 mx-auto">
                    <i class="material-icons-outlined">directions_transit</i>
                </button>
                Pengangkutan
            </div>
        </div>
    </div>
</div>


<div class="pr-stats-bar my-5" data-aos="fade-up">
    <div class="container p-5">
        <div class="row justify-content-center">
            <div class="col text-center pr-stats-item">
                <div class="count" style="font-size: 45px; font-weight: bold;">13093</div>
                <div style="font-size: 20px;">Jumlah Set Data</div>
            </div>
            <div class="col text-center pr-stats-item">
                <div class="count" style="font-size: 45px; font-weight: bold;"><?php echo "200"; ?></div>
                <div style="font-size: 20px;">Jumlah Pembekal Set Data</div>
            </div>
            <div class="col text-center pr-stats-item">
                <div class="count" style="font-size: 45px; font-weight: bold;">142</div>
                <div style="font-size: 20px;">Jumlah Pelawat</div>
            </div>
        </div>
    </div>
    <div class="col text-right pr-stats-item" style="font-size: 18px;">Statistik Sehingga 12/03/2020</div>
</div>


<div class="pr-diagram-bar my-5" data-aos="fade-up">
    <div class="container">
        <div class="row justify-content-center">
            <div class="text-center my-3 pr-title">
                <span>STATISTIK</span> DATA TERBUKA  
            </div>
        </div>
        <div class="row justify-content-center my-4">
            <div class="col-6">
                <div class="card">
                    <div class="card-body">
                        <canvas id="myChart" width="100%"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="card">
                    <div class="card-body">
                        <canvas id="myChart1" width="100%"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-center my-4">
            <div class="col-6">
                <div class="card">
                    <div class="card-body">
                        <canvas id="myChart2" width="100%"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="card">
                    <div class="card-body">
                        <canvas id="myChart3" width="100%"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

AOS.init();

var bView = false;

$(document).scroll(function() {
  var y = $(this).scrollTop();
  if (y > 600) {

    if(!bView)
    {
        $('.count').each(function () {
            $(this).prop('Counter',0).animate({
                Counter: $(this).text()
            }, {
                duration: 3000,
                easing: 'swing',
                step: function (now) {
                    $(this).text(Math.ceil(now));
                }
            });
        });

        bView = true;
    }

  } else {
    // $('.bottomMenu').fadeOut();
  }

});



var ctx = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
        datasets: [{
            label: '# of Votes',
            data: [12, 19, 3, 5, 2, 3],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});

var ctx = document.getElementById('myChart1').getContext('2d');
var myDoughnutChart = new Chart(ctx, {
    type: 'doughnut',
    data: data = {
    datasets: [{
        data: [25, 20, 30, 45, 58, 33],
        backgroundColor: [
                'rgba(255, 99, 132, 0.9)',
                'rgba(54, 162, 235, 0.9)',
                'rgba(255, 206, 86, 0.9)',
                'rgba(75, 192, 192, 0.9)',
                'rgba(153, 102, 255, 0.9)',
                'rgba(255, 159, 64, 0.9)'
            ],
    }],
    labels: [
        'Red',
        'Yellow',
        'Blue'
    ]
    },
    // options: options
});


var ctx = document.getElementById('myChart2').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
        datasets: [{
            label: '# of Votes',
            data: [12, 19, 3, 5, 2, 3],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});


var ctx = document.getElementById('myChart3').getContext('2d');
var myDoughnutChart = new Chart(ctx, {
    type: 'doughnut',
    data: data = {
    datasets: [{
        data: [25, 20, 30, 45, 58, 33],
        backgroundColor: [
                'rgba(255, 99, 132, 0.9)',
                'rgba(54, 162, 235, 0.9)',
                'rgba(255, 206, 86, 0.9)',
                'rgba(75, 192, 192, 0.9)',
                'rgba(153, 102, 255, 0.9)',
                'rgba(255, 159, 64, 0.9)'
            ],
    }],
    labels: [
        'Red',
        'Yellow',
        'Blue'
    ]
    },
    // options: options
});
</script>
</html>
