<br>
<div class="container">
    <h2 style="text-align:justify"><span style="color:#000000"><span style="font-size:16px"><span style="font-family:verdana,geneva,sans-serif"><h3>Tentang Portal Terbuka Malaysia</h3></span></span></span></h2>

    <p style="text-align:justify"><span style="color:#000000"><span style="font-size:14px"><span style="font-family:verdana,geneva,sans-serif">Portal Data Terbuka Sektor Awam, data.gov.my yang dibangunkan secara dalaman telah dilancarkan oleh YB. Datuk Joseph Entulu Anak Belaun, Menteri di Jabatan Perdana Menteri di &nbsp;Persidangan CIO ASEAN&nbsp; 2014. Portal ini merupakan <em>one-service-centre</em> kepada rakyat mencapai dan memuat turun dataset data terbuka Kerajaan secara dalam talian.</span></span></span></p>

    <p style="text-align:justify"><span style="color:#000000"><span style="font-size:14px"><span style="font-family:verdana,geneva,sans-serif">Portal ini telah dinaik taraf dengan penambahan ciri-ciri baharu mengikut trend data terbuka peringkat global bagi memudahkan capaian ke atas set data terdapat dalam portal ini. &nbsp;Portal ini telah dinaik taraf dan <em>Go Live</em> pada 23&nbsp;Mei 2016.</span></span></span></p>

    <p style="text-align:justify"><span style="color:#000000"><span style="font-size:14px"><span style="font-family:verdana,geneva,sans-serif">Objektif Data Terbuka:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></span></span></p>

    <ul>
        <li style="text-align:justify"><span style="color:#000000"><span style="font-size:14px"><span style="font-family:verdana,geneva,sans-serif">Membolehkan data terbuka dikongsi secara lebih meluas dan meningkatkan ketelusan perkhidmatan Kerajaan;</span></span></span></li>
        <li style="text-align:justify"><span style="color:#000000"><span style="font-size:14px"><span style="font-family:verdana,geneva,sans-serif">Memberi peluang kepada rakyat dan komuniti bisnes bagi meningkatkan kreativiti dan inovasi di dalam penciptaan produk baharu;</span></span></span></li>
        <li style="text-align:justify"><span style="color:#000000"><span style="font-size:14px"><span style="font-family:verdana,geneva,sans-serif">Menyediakan platform kepada rakyat bagi mendapatkan maklumat dari sumber rasmi Kerajaan dan sebagai saluran mendapatkan maklum balas daripada rakyat;</span></span></span></li>
        <li style="text-align:justify"><span style="color:#000000"><span style="font-size:14px"><span style="font-family:verdana,geneva,sans-serif">Menjimatkan masa capaian dataset data terbuka oleh rakyat;dan</span></span></span></li>
        <li style="text-align:justify"><span style="color:#000000"><span style="font-size:14px"><span style="font-family:verdana,geneva,sans-serif">Menjimatkan kos pembangunan aplikasi agensi Kerajaan.</span></span></span></li>
    </ul>
</div>