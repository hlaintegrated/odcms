<?php if ($section === 'new'): ?>
<style>
            .title {
                font-weight: bold;
            }
            .mandatory {
                color: #ff0000;
            }
            .eg {
                font-size: 0.9rem;
            }
            .btn-text {
                font-weight:bold; 
                width:90px;
            }
            .mb0 {
                font-weight: bold; 
                color: #6c757d;
            }
</style>
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script>
$(document).ready(function(){
    $(".reset-btn").click(function(){
        $("#Form").trigger("reset");
    });
});
</script>
<br>
<div class="container contact">
            <div class="row">
                <div class="col-lg-9">
                    <div class="card">
                        <div class="card-header "><b>Permohonan Set Data</b></div>
                        <div class="card-body">
                                <form action="<?php echo base_url();?>ticket/addrequest" method="POST" id="Form">
                                    <!-- Textarea Nama -->
                                    <div class="row">                          
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label for ="request_by" class ="col-form-label title">Nama<span class="mandatory">*</span></label>
                                            </div>
                                        </div>
                                        
                                        <div class="col-lg-9">
                                            <div class="form-group">
                                                <input type="text" name="request_by" class ="form-control eg" placeholder="Cth: Syed Imran Hafizzudin " value=""></input> 
                                                
                                            </div>
                                        </div>                                        
                                    </div>
                                    <!-- Textarea No. telefon -->
                                    <div class="row">                          
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label for ="contact" class ="col-form-label title">No. Telefon<span class="mandatory">*</span></label>
                                            </div>
                                        </div>
                                        <div class="col-lg-9">
                                            <div class="form-group">
                                                <input type="text" name="contact" class ="form-control eg" placeholder="Cth: 01123374868" value=""></input> 
                                             
                                            </div>
                                        </div>
                                    <!-- Textarea E-mel -->
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label for ="email" class ="col-form-label title">E-mel<span class="mandatory">*</span></label>
                                            </div>
                                        </div>
                                        <div class="col-lg-9">
                                            <div class="form-group">
                                                <input type="text" name="email" class="form-control eg" placeholder="Cth: syedimran@domain.com" value=""></input> 
                                               
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Textarea Tajuk Set Data -->
                                    <div class="row">                          
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label for ="title" class="col-form-label title">Tajuk Set Data<span class="mandatory">*</span></label>
                                            </div>
                                        </div>
                                        <div class="col-lg-9">
                                            <div class="form-group">
                                                <input type="text" name="title" class="form-control eg" placeholder="Cth: Lokaliti Hotspot Denggi di Malaysia mengikut Negeri" value=""></input>
                                             
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Textarea Keterangan Set Data -->
                                    <div class="row">                          
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label for ="description" class ="col-form-label title">Keterangan Set Data<span class="mandatory">*</span></label>
                                            </div>
                                        </div>
                                        <div class="col-lg-9">
                                            <div class="form-group">
                                                <textarea rows="3" name="description" class="form-control eg" placeholder="Cth: Senarai denggi di Malaysia dari tahun 2000 hingga 2016 oleh negeri"></textarea> 
                                            
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Textarea Justifikasi Permohonan -->
                                    <div class="row">                          
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label for ="justification" class ="col-form-label title">Justifikasi Permohonan<span class="mandatory">*</span></label>
                                            </div>
                                        </div>
                                        <div class="col-lg-9">
                                            <div class="form-group">
                                                <textarea rows="3" name="justification" class ="form-control eg" placeholder="Cth: Memerlukan senarai denggi di Malaysia untuk tujuan laporan penyelidikan universiti"></textarea> 
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Dropdown Penyumbang Set Data-->
                                    <div class="row">                          
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label for ="oid" class ="col-form-label title">Penyumbang Set Data<span class="mandatory">*</span></label>
                                            </div>
                                        </div>
                                        <div class="col-lg-9">
                                            <div class="form-group">
                                                <select id="report" name="oid" class="form-control  eg" value="">
                                                <option selected disabled>Sila Pilih</option>       
                                                <?php foreach ($sources as $src): ?>             
                                                    <option value="<?php echo $src->oid ?>"><?php echo $src->name_bm ?></option>
                                                <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                    <div class="col-lg-4"></div>  
                                    <!-- Butang Batal Permohonan -->                     
                                        <div class="col-lg-2">
                                            <div class="form-group">
                                                <button type="button" class="btn btn-primary btn-md eg btn-text reset-btn">Reset</button>
                                            </div>
                                        </div>
                                    <!-- Butang Hantar Permohonan -->
                                        <div class="col-lg-2">
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-primary btn-md eg btn-text">Hantar</button> 
                                            </div>
                                        </div>
                                        <div class="col-lg-4"></div> 
                                    </div>
                                    
                                </form>
                            </div>
                    </div>
                </div>
                <!-- Ruang Panduan -->
                <div class="col-lg-3 mb0">
                    <div><img src="<?= base_url();?>assets/img/info-icon.png"></div><br>
                    <div>
                        <span>Sila isikan borang di sebelah untuk memohon set data</span>
                    </div>  
                    <br>
                    <div>
                        <span>NOTA: Medan bertanda <span class="mandatory">*</span> adalah mandatori</span>
                    </div>
                </div>
            </div>
        </div>

<?php endif; ?>
<?php if ($section === 'check'): ?>

  <div class="jumbotron">
  <h1>check semak ticket</h1>
  </div>

<?php endif; ?>
<?php if ($section === 'email'): ?>

<br>
<table align="center" style="margin: 10px auto;">
    <tr>
        <td>
            <img src="<?= base_url();?>assets/img/dtsa.png">
        </td>
    </tr>
</table>

 <p style='color:#4d4d4d;font-family:Oxygen, Helvetica neue, sans-serif;font-size:18px;font-weight:100;line-height:30px;text-align:center;'>Permohonan anda telah berjaya dihantar!</p><br>
 <td>
 <div>
 <span style='font-weight:700; color: #838282; font-size: 15px;'> Tiket ID: </span>

 </div>
 </td>
 <div class="panel-body">
</div>

<?php endif; ?>   


