<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Authenticate {

    /**
     * Validate Dummy : To validate dummy user. This is for any user that are outside the Active Directory authentication
     * @author Ikhwan <mohamad.ikhwan@pslgroup.com>
     * @since   2013-01-21
     * @access public
     * @param string $username the username of the user
     * @param string $password the password of the user
     * @return boolean $dummy THe status of the authentication
     */
    function validate_dummy($username, $password){
      $CI =& get_instance();
      $CI->load->library('session');

      $dummy = FALSE;

      $tempdb[0]['username'] = 'mohamad.ikhwan';
      $tempdb[0]['password'] = '098f6bcd4621d373cade4e832627b4f6';
      $tempdb[1]['username'] = 'tibcouser';
      $tempdb[1]['password'] = '5b649c322e321396ef840f7ee91ed357';

      foreach($tempdb as $dummy) {
        if ($username == $dummy['username'] && md5($password) == $dummy['password']){

          //start initializing credential session
          $cred['username'] = $username;
          $cred['email'] = ($username == 'tibcouser') ? 'ajeong@tibco.com' : $username . '@pslgroup.com'; //hard-coded assign the email to tibco representative
          $cred['logged_in'] = TRUE;

          $CI->session->set_userdata($cred);

          $dummy = TRUE;
          break;

        } else {

          $dummy = FALSE;
        }

      }

      return $dummy;

    }

    /**
     * Is Logged In : To check whether the user is logged in or not
     * @author Ikhwan <mohamad.ikhwan@pslgroup.com>
     * @since   2013-01-21
     * @access public
     * @return boolean $dummy The status of the login session
     */
    function isLoggedIn(){
      $CI =& get_instance();
      $CI->load->library('session');

      if ($CI->session->userdata('logged_in') && $CI->session->userdata('logged_in') == TRUE){
        return TRUE;
      } else {
        return FALSE;
      }
    }

    /**
     * Is SuperUser : To check whether the user is SuperUser in or not
     * @author Ikhwan <mohamad.ikhwan@pslgroup.com>
     * @since   2015-11-24
     * @access public
     * @return boolean The status of the credential
     */
    function isSuperUser(){
      $CI =& get_instance();
      $CI->load->library('session');

      if (($CI->session->userdata('logged_in') && $CI->session->userdata('logged_in') == TRUE) && ($CI->session->userdata('is_su') && $CI->session->userdata('is_su') == TRUE) && ($CI->session->userdata('is_admin') && $CI->session->userdata('is_admin') == TRUE)){
        return TRUE;
      } else {
        return FALSE;
      }
    }

    /**
     * Is Admin : To check whether the user is an admin in or not
     * @author Ikhwan <mohamad.ikhwan@pslgroup.com>
     * @since   2013-01-21
     * @access public
     * @return boolean $dummy The status of the credential
     */
    function isAdmin(){
      $CI =& get_instance();
      $CI->load->library('session');

      if (($CI->session->userdata('logged_in') && $CI->session->userdata('logged_in') == TRUE) && ($CI->session->userdata('is_admin') && $CI->session->userdata('is_admin') == TRUE)){
        return TRUE;
      } else {
        return FALSE;
      }
    }

    /**
     * LDAP Login : To validate AD user.
     * @author Ikhwan <mohamad.ikhwan@pslgroup.com>
     * @since   2013-01-21
     * @access public
     * @param string $email the email of the user
     * @param string $password the password of the user
     * @return boolean $user_allowed The status of the authentication
     */
    function ldap_login($email, $password) {

      $CI =& get_instance();
      $CI->load->library('session');

      $user_allowed = FALSE;

      $dummy_user = FALSE;

      if (defined('ENVIRONMENT') AND ENVIRONMENT != 'production') {
        $dummy_user = self::validate_dummy($email, $password);

      } else {
        $dummy_user = FALSE;
      }

      if (!$dummy_user){

        $accessgroupid = $this->check_user_exists($email);

        if ($accessgroupid == FALSE) {
          $user_allowed = FALSE;
        } else {
          // ldap configuration settings
          $ldap_settings = array (
              // ldap host
            'host' => $CI->config->item('LDAP_HOST'),
              // domain component
            'dc' => $CI->config->item('LDAP_DOMAIN_COMPONENT'),
              // group name
            'group' => $CI->config->item('LDAP_GROUP')
            );

              // connect to ldap server
          $ds = ldap_connect($ldap_settings['host']) or die ("Cant connect!");

          if($ds) {
            // connected successfully

            // specify protocol version
            ldap_set_option($ds, LDAP_OPT_PROTOCOL_VERSION, 3);

            // disable follow referral option
            ldap_set_option($ds, LDAP_OPT_REFERRALS, 0);

            $base_dn = 'dc='.implode(',dc=', $ldap_settings['dc']);

            $bind = @ldap_bind($ds, $email , $password);

            if($bind) {
              // bind was successful

              // filter by user object type with the provided email
              $filter = "(&(objectClass=user)(objectCategory=person)(userprincipalname=$email))";

              $search=@ldap_search($ds, $base_dn, $filter);
              if($search) {

                $number_returned = ldap_count_entries($ds, $search);
                if($number_returned == 1) {

                  // get user's info
                  $info = ldap_get_entries($ds, $search);


                  // check if user belongs to
                  $memberof = $info[0]['memberof'];
                  $required_group = 'CN=' . $ldap_settings['group'];

                  // go through the list of groups user belongs to
                  // check if required group exists in the list
                  foreach ($memberof as $group) {
                    $hasGroup = stripos($group, $required_group);
                    if($hasGroup !== FALSE) {

                      // user has the required group, can stop looking
                      //start initializing credential session
                      $cred['username'] = trim(preg_replace('/@.*$/i', '', $email));
                      $cred['email'] = $email;
                      $cred['logged_in'] = TRUE;
                      $cred['accessgroupid'] = $accessgroupid;

                      //get access group type for the user

                      foreach ($accessgroupid as $xs) {
                        $accessgrouptype = $this->get_access_grouptype($xs);

                        //set session for superuser
                        if ($accessgrouptype == 'superuser'){
                          $cred['is_su'] = TRUE;
                          $cred['is_admin'] = TRUE;
                        }
                        //set session for admins
                        if ($accessgrouptype == 'admin'){
                          $cred['is_admin'] = TRUE;
                        }

                      }

                      $CI->session->set_userdata($cred);

                      $user_allowed = TRUE;
                      break;
                    }
                  }
                }
              } // end if search worked
            } // end if binded
          } // end if connected

          // close ldap connection
          ldap_close($ds);

        }

    } else {

      $user_allowed = $dummy_user;

    }

    return $user_allowed;
  }

  function check_user_exists($email) {
    $CI =& get_instance();
    $CI->load->model('admin_model');

    $exists = $CI->admin_model->check_user_exists($email);

    return $exists;
  }

  function get_access_grouptype($accessgroupid) {
    $CI =& get_instance();
    $CI->load->model('admin_model');

    $exists = $CI->admin_model->get_access_grouptype($accessgroupid);

    return $exists;
  }

  function get_site_access($accessgroupids) {
    $CI =& get_instance();
    $CI->load->model('admin_model');

    $group_access = $CI->admin_model->get_access_group($accessgroupids);

    $site_access = array();
    $access = array();

    if (array_key_exists('accessiblepages', $group_access)) {

        $site_access = json_decode($group_access['accessiblepages']);

    } else {

      foreach ($group_access as $sites) {
        if (! empty($sites['accessiblepages'])) {
          $access = json_decode($sites['accessiblepages']);
        //flip to eliminate duplicate site access
          $site_access += array_flip($access);
        }

      }
      //get back the keys, and reconstruct the array again
      $site_access = array_keys($site_access);

    }

    return $site_access;
  }

  function allow($uri, $auto_redirect = TRUE) {

    static $site_access;

    if (empty($site_access)) {
      $CI =& get_instance();
      $CI->load->library('session');
      $CI->load->helper('url');

      $site_access = $this->get_site_access($CI->session->userdata('accessgroupid'));
    }

    $key = array_search($uri, $site_access);

    if ($key === FALSE) {
      if ($auto_redirect) {
        $CI->session->set_flashdata('notif','error:- You don\'t have proper access right for this page');
        redirect('main/index');
      } else {
        return FALSE;
      }
    } else {
      return TRUE;
    }

  }

  function construct_accessible_nav_menu() {

    static $menu;

    if (empty($menu)) {
      $CI =& get_instance();
      $CI->load->library('session');
      $CI->load->model('admin_model');

      $site_registry = $CI->admin_model->get_site_registry();
      $site_access = $this->get_site_access($CI->session->userdata('accessgroupid'));

      foreach ($site_access as $uri) {
          $admin_str = strpos($uri, 'admin');
          if ($admin_str === FALSE) {
              $menu['pages'][$uri] = $site_registry[$uri];
          } else {
              $menu['admin'][$uri] = $site_registry[$uri];
          }
      }
      if (! empty($menu['pages'])) asort($menu['pages']);
      if (! empty($menu['admin'])) asort($menu['admin']);

    }
    return $menu;

  }

  function get_accessible_nav_menu($filterby = NULL){

    if ($filterby != NULL) {
      $menu = $this->construct_accessible_nav_menu();
      return $menu[$filterby];
    } else {
      return $this->construct_accessible_nav_menu();
    }

  }

  function get_field_access($accessgroupids) {
    $CI =& get_instance();
    $CI->load->model('admin_model');

    $group_access = $CI->admin_model->get_access_group($accessgroupids);

    $field_access = array();
    $access = array();

    if (array_key_exists('accessiblefields', $group_access)) {

        $field_access = json_decode($group_access['accessiblefields']);

    } else {

      foreach ($group_access as $field) {

        if (! empty($field['accessiblefields'])){
          $access = json_decode($field['accessiblefields']);
          //flip to eliminate duplicate site access
          $field_access += array_flip($access);
        }

      }
      //get back the keys, and reconstruct the array again
      $field_access = array_keys($field_access);

    }

    return $field_access;
  }

  function allow_fields($fieldname) {

    static $field_access;

    $key = FALSE;

    if (empty($field_access)) {
      $CI =& get_instance();
      $CI->load->library('session');
      $CI->load->helper('url');

      $field_access = $this->get_field_access($CI->session->userdata('accessgroupid'));

    }

    if (! empty($field_access)) {
      $key = array_search($fieldname, $field_access);
    }

    if ($key === FALSE) {
      return FALSE;
    } else {
      return TRUE;
    }

  }

}

/* End of file Authenticate.php */
/* Location: ./system/application/libraries/Authenticate.php */